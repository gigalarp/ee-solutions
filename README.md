# Solutions for protostar and fusion challenges from Andrew Griffiths exploit excercises

## Protostar

### All challenges done

http://exploit.education/protostar/

Protostar introduces the following in a friendly way:

    Network programming
    Byte order
    Handling sockets
    Stack overflows
    Format strings
    Heap overflows
    
## Fusion

### First five challenges completed

http://exploit.education/fusion/

Fusion is the next step from the protostar setup, and covers more advanced styles of exploitation, and covers a variety of anti-exploitation mechanisms such as:

    Address Space Layout Randomisation
    Position Independent Executables
    Non-executable Memory
    Source Code Fortification (_DFORTIFY_SOURCE=)
    Stack Smashing Protection (ProPolice / SSP)

In addition to the above, there are a variety of other challenges and things to explore, such as:

    Cryptographic issues
    Timing attacks
    Variety of network protocols (such as Protocol Buffers and Sun RPC)


