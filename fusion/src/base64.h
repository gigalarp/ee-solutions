#ifndef __BASE64_H_
#define __BASE64_H_

unsigned char * base64_encode(const unsigned char *, size_t, size_t *);
unsigned char * base64_decode(const unsigned char *, size_t, size_t);

#endif /* __BASE64_H_ */
