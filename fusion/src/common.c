#include <sys/types.h>    
#include <sys/socket.h>    
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>    
#include <stdlib.h>    
#include <string.h>    
#include <errno.h>    
#include <assert.h>    
#include <unistd.h>
#include <ctype.h>

#include "common.h"
#include "colors.h"

void fatal(const char *str)
{
    fprintf(stderr, "%s[FATAL ERROR] %s (errno = %d)%s\n", COLOR_RED, str, errno, COLOR_RESET);
    exit(1);
}

void print_info_message(char *msg)
{
	printf("[%s+%s] %s\n", COLOR_BLUE, COLOR_RESET, msg);
}

void get_port_address_from_args(int argc, char **argv, unsigned long *addr, unsigned short *port)
{
	if (argc != 3) {
		printf("Usage: %s <IP> <PORT>\n", argv[0]);
		exit(1);
	}

	if (inet_pton(AF_INET, argv[1], addr) != 1)
		fatal("invalid IP address.");
	if (atoi(argv[2]) > 65535)
		fatal("invalid port number.");
	*port = atoi(argv[2]);
}

int init_connection(unsigned long addr, unsigned short port)
{
	int fd;
	/* These aren't saved because we don't use them after this */
	struct sockaddr_in saddr;
	struct in_addr iaddr;

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		fatal("while creating a socket.");

	iaddr.s_addr = addr;
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr = iaddr;

	if (connect(fd, (const struct sockaddr *)&saddr, sizeof(struct sockaddr)) < 0)
		fatal("while connecting to the server.");

	return fd;
}

void recv_buf(int fd, unsigned char *buf, unsigned int bytes, unsigned int buf_size)
{
	memset(buf, '\0', buf_size);

	if (!bytes || bytes > buf_size)
		recv(fd, buf, buf_size, 0);
	else
		recv(fd, buf, bytes, 0);
}

void print_uint_array(unsigned int a[], unsigned int len)
{
	unsigned int i;

	printf("\t");
	for (i = 0; i < len; i++) {
		printf("0x%08x ", a[i]);

	if (i % 10 == 9)
		printf("\n\t");
	}
	printf("\n");
}

void print_uchar_buffer(unsigned char *buf, unsigned int len)
{
	unsigned int i, j;

	printf("\t");
	for (i = 0; i < len; i++) {
		printf("%02x ", buf[i]);

		if (i % 10 == 9) {
			printf("\t\t| ");
			for (j = 0; j < i % 10; j++)
				printf("%c", isprint(buf[i - ((len - i) % 10) + j]) && buf[i - ((len - i) % 10) + j] != '\0' ? buf[i - ((len - i) % 10) + j] : '.');
				//HUOM: isprint pit'' nollatavua tulostettavana, FIXME korjaa ylempi rivi, ett' sen sijaan tulostetaan piste
			printf(" |\n\t");
		}
	}
	printf("\n");
}

/* Again bad practices (trusting len), but whatever */
void send_message_E(int fd, unsigned char *msg, unsigned int len)
{
	send(fd, "E", 1, 0);
	send(fd, &len, sizeof(unsigned int), 0);
	send(fd, msg, len, 0);
}

/* Note: Hangs when user gives a command that doesn't output anything, because the recv waits for input. Solution: user can chain commands. e.g. 'cd xyz; ls' */
void interact_with_remote_shell(int fd, int root)
{
	char send_buffer[1024];
	char recv_buffer[4096];

	while (1) { /* cd doesn't work for some reason.. bc IT DOESN'T OUTPUT ANYTHING. also doesn't like nonexistent commands */
		printf("%s%c%s ", COLOR_RED, root ? '#' : '$', COLOR_RESET);    
		memset(send_buffer, '\0', sizeof(send_buffer));    
		memset(recv_buffer, '\0', sizeof(recv_buffer)); /* recv() doesn't add a NULL-byte to the end of the string */    
		if (!fgets(send_buffer, 1024, stdin))
			fatal("while trying to read input from stdin");
		if (strlen(send_buffer) == 0)    
			continue;    

		strcat(send_buffer, "\n");
		send(fd, send_buffer, strlen(send_buffer), 0);    

		if (strncmp(send_buffer, "exit", 4) == 0)    
			break;    

		recv(fd, recv_buffer, sizeof(recv_buffer), 0);
		printf("%s", recv_buffer);    
	}
}

