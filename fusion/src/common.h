#ifndef _COMMON_H_
#define _COMMON_H_

#include "colors.h"

void fatal(const char *);
void print_info_message(char *);
void get_port_address_from_args(int, char **, unsigned long *, unsigned short *);
int init_connection(unsigned long, unsigned short); /* address must be in network order before passing it here, returns a file descriptor */
void print_uint_array(unsigned int [], unsigned int);
void print_uchar_buffer(unsigned char *, unsigned int);
void send_message_E(int fd, unsigned char *, unsigned int);
void interact_with_remote_shell(int, int);

#endif /* _COMMON_H_ */
