#ifndef CURSOR_H
#define CURSOR_H

#define MOVE_CURSOR_UP(n)       printf("\033[" #n "A")
#define MOVE_CURSOR_DOWN(n)     printf("\033[" #n "B")
#define MOVE_CURSOR_FORWARD(n)  printf("\033[" #n "C")
#define MOVE_CURSOR_BACKWARD(n) printf("\033[" #n "D")

#endif /* CURSOR_H */

