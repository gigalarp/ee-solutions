#ifndef DEBUG_H
#define DEBUG_H

#ifdef DBG

	/* Print debug message */
	#define print_debug_message(fmt, args...) \
		printf("[%sDBG%s]%s " fmt "%s\n" , COLOR_RED, COLOR_RESET, COLOR_YELLOW, ##args, COLOR_RESET)

#endif

#endif /* DEBUG_H */
