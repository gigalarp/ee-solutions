#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>

#define GETLINE_SUCCESS 0
#define GETLINE_FAIL -1

#define LINE_LENGTH 128

void fatal(const char *);
int sock_getline(const int, unsigned char *);

/*
* NOTE: see ip(7) and tcp(7)
*/

int main (int argc, char **argv)
{
	int fd;
	unsigned short port;
	unsigned long addr;
	struct sockaddr_in saddr;
	struct in_addr iaddr;

	unsigned char buffer[LINE_LENGTH + 1];

	if (/*!argv[2]*/argc != 3) {
		printf("Usage: %s <IP> <PORT>\n", argv[0]);
		exit(1);
	}

	//TODO!: Validate the value of the port before converting it
	if (addr = inet_pton(AF_INET, argv[1], &addr) != 1)
		fatal("No valid IP address were given.");
	port = atoi(argv[2]); //bad


	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		fatal("while creating a socket.");

	iaddr.s_addr =	htonl(addr);
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr = iaddr;

	if (connect(fd, (const struct sockaddr *)&saddr, sizeof(struct sockaddr)) < 0)
		fatal("while connecting to the server.");

	/* the main logic */
	if (sock_getline(fd, buffer) != GETLINE_SUCCESS)
		fatal(" while caliing sock_getline().");

	printf("received this line: \"%s\"\n", buffer);
	
	close(fd);
	return 0;
}

void fatal(const char *str)
{
	fprintf(stderr, "[FATAL ERROR] %s\n", str);
	exit(1);
}

/* Read one line from a socket */
int sock_getline(const int fd, unsigned char *str)
{
	int i, nr; /* nr = num_read */
	unsigned char c;

	for (i = 0; i < LINE_LENGTH - 1; i++) {
		nr = recv(fd, &c, sizeof(unsigned char), 0);

		switch (nr) {
			case -1:
				fatal("while recv().");
				break;
			case 0:
				fatal("the peer has performed an orderly shutdown. @recv()");
				break;
			case 1:
				if (c == '\n') {
					str[i] = '\0';
					return GETLINE_SUCCESS;
				}
				str[i] = c;
				break;
			default:
				fatal("unknown error! @recv()");
				break;
		}
	}

	return GETLINE_FAIL;
}
