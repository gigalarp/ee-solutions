#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define GETLINE_SUCCESS 0
#define GETLINE_FAIL -1

#define LINE_LENGTH 128

void fatal(const char *);
int sock_getline(const int, unsigned char *);

/*
* NOTE: see ip(7) and tcp(7)
*/

int main (int argc, char **argv)
{
	int i;
	int fd;
	unsigned short port;
	unsigned long addr;
	struct sockaddr_in saddr;
	struct in_addr iaddr;

	unsigned char buffer[LINE_LENGTH + 1];
	unsigned int wanted;
	unsigned char buf[12];

	if (argc != 3) {
		printf("Usage: %s <IP> <PORT>\n", argv[0]);
		exit(1);
	}

	if (addr = inet_pton(AF_INET, argv[1], &addr) != 1)
		fatal("No valid IP address were given.");
	port = atoi(argv[2]); //BAD! CAN OVERFLOW!


	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		fatal("while creating a socket.");

	iaddr.s_addr =	htonl(addr);
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr = iaddr;

	if (connect(fd, (const struct sockaddr *)&saddr, sizeof(struct sockaddr)) < 0)
		fatal("while connecting to the server.");

	/* the main logic */
	recv(fd, &wanted, 4, 0);
	
	printf("\t\tThe wanted number is %u (0x%x)!\n", wanted, wanted);
	
	/* this is probably bad */
	memset(buf, '\0', 12);
	sprintf(buf, "%d", wanted);
	printf("\t\tSending number %u (0x%x)..\n", atoi(buf), atoi(buf));
	for (i = 0; i < 12; i++)
		if(buf[i] == '\0')
			break;

	buf[i++] = '\r';
	buf[i] = '\n';
	buf[11] = '\0';
	send(fd, buf, 10, 0);
	printf("Data sent to server successfully.\n");

	if (sock_getline(fd, buffer) != GETLINE_SUCCESS)
		fatal(" while calling sock_getline().");
	printf("\treceived this line: \"%s\"\n", buffer);

	close(fd);
	return 0;
}

void fatal(const char *str)
{
	fprintf(stderr, "[FATAL ERROR] %s (errno = %d)\n", str, errno);
	exit(1);
}

/* Read one line from a socket */
int sock_getline(const int fd, unsigned char *str)
{
	int i, nr; /* nr = num_read */
	unsigned char c;

	for (i = 0; i < LINE_LENGTH - 1; i++) {
		nr = recv(fd, &c, sizeof(unsigned char), 0);

		switch (nr) {
			case -1:
				if (errno == 104)
					fatal("while recv(). (Connection reset by peer)");

				fatal("while recv().");
				break;
			case 0:
				fatal("the peer has performed an orderly shutdown. @recv()");
				break;
			case 1:
				if (c == '\n') {
					str[i] = '\0';
					return GETLINE_SUCCESS;
				}
				str[i] = c;
				break;
			default:
				fatal("unknown error! @recv()");
				break;
		}
	}

	return GETLINE_FAIL;
}

unsigned int get_wanted(const char *str)
{
	char buffer[5];
	int i;

	memset(buffer, '\0', 5);

	for (i = 0; i < 4; i++)
		buffer[i] = str[i];

	/* we expect the it worked here */
	return (unsigned int) atoi(buffer); /* NOTE: atoi does NOT do error handling! */
}

